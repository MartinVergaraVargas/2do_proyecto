#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Dialogos():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()
        
        self.builder.add_from_file("./proyecto_PDB.ui")

        self.dialogo = self.builder.get_object(object_name)
