#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import pymol
from biopandas.pdb import PandasPdb

import os



class Seleccionador():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()        
        self.builder.add_from_file("./proyecto_PDB.ui")

        self.dialogo = self.builder.get_object("Chooser")
        
        
        # Botones de ventana FileChooser
        Seleccionar = self.builder.get_object("Seleccionar")
        Seleccionar.connect('clicked', self.generar_imagen)

        Cancelar = self.builder.get_object("Cancelar")
        Cancelar.connect('clicked', self.boton_cancelar)
        
        
    def generar_imagen(self, btn=None):
        self.archivo = self.dialogo.get_file()
        self.nombreArchivo = self.archivo.get_basename()
        print(self.nombreArchivo)
        
        nombre_archivo, extension_archivo = os.path.splitext(self.nombreArchivo)
        print(extension_archivo)                # Con este fragmento de codigo y el "import os" se separan
                                                # correctamente la extension del nombre del archivo
        print(nombre_archivo)
        
                
        self.ruta = self.dialogo.get_current_folder()
        print(self.ruta)
        print("##########################################################################")
        print("##########################################################################")
        
        
        if (extension_archivo == ".pdb"):       # La idea de este if es reconocer si el archivo seleccionado es:
                                                # .pdb: se generan los archivos correspondientes y su imagen.
                                                # .png: se muestra en pantalla.
                                                # .pdb.gz: se generan los archivos con el codigo alternativo de la pagina de biopandas.
                                                
                                                # El problema que enfrento es que no logro retornar la extension del archivo desde la ventana FileChooser
                                                # hasta el programa principal.
                        
            # Información PDB
            ppdb = PandasPdb()
            ppdb.read_pdb(self.ruta + "/" + self.nombreArchivo)
            for i in ppdb.df.keys():
                print (i)
                print(ppdb.df[i])
                print("\n")
                
                if (i != "ANISOU"): # PROBLEMA, se generan todos los archivos pero no el de ANISOU
                    nom = nombre_archivo + "_" + i + ".pdb"
                    ppdb.to_pdb(path = (self.ruta + "/" + nom),
                            records = [i],
                            gz = False,
                            append_newline = True)
            
            # Imagen PDB
            pymol.cmd.load(self.ruta + "/" + self.nombreArchivo, self.nombreArchivo)
            pymol.cmd.disable("all")
            pymol.cmd.enable(self.nombreArchivo)
            print(pymol.cmd.get_names())
            pymol.cmd.hide("all")
            pymol.cmd.show("cartoon")
            pymol.cmd.set("ray_opaque_background", 0)
            pymol.cmd.pretty(self.nombreArchivo)
            pymol.cmd.png("%s.png" % (self.nombreArchivo))
            pymol.cmd.quit()
            
        elif (extension_archivo == ".png"):
            return extension_archivo
            

        
        
        self.dialogo.destroy()
        
    def boton_cancelar(self, btn=None):
        self.dialogo.destroy()
        

